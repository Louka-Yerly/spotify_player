#!/usr/bin/python3

from project import db, create_app, models
from werkzeug.security import generate_password_hash
from project import models
app = create_app()
db.create_all(app=app) # pass the create_app result so Flask-SQLAlchemy gets the configuration.
new_user = models.User(username="test",
                       password=generate_password_hash("123", method='sha256'), 
                       super_admin=True)
with app.app_context():
    db.session.add(new_user)
    db.session.commit()