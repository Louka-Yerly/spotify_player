
class Track():
    def __init__(self, artist="", track="", album="", uri="", album_image="", duration=0):
        self.artist = artist
        self.track = track
        self.album = album
        self.uri = uri
        self.album_image = album_image
        self.duration = duration
        

    def print_track(self):
        print(f'Artist: {self.artist}')
        print(f'Album:  {self.album}')
        print(f'Track:  {self.track}')
        print(f'uri:    {self.uri}')
    