#!/usr/bin/python3

from my_track import Track
from my_spotify import Spot

s = Spot(username="yerlylouka")

def show_tracks(tracks):
    for i in range(len(tracks)):
        track = tracks[i]
        if isinstance(track, Track):
            print(f"({ i+1 })")
            track.print_track()
            print()


def ask_user():
    while(True):
        name = str(input("Track name: "))
        if name == "n" :
            s.skip_track()
            continue
        elif name == "l":
            tracks = s.get_queue()
            print("Queue:")
            show_tracks(tracks)
            continue
        # elif name.__contains__("d;"):
        #     name = name.split(";")
        #     s.delete_track(name[1])
        #     continue
        elif name == "d":
            s.select_device()
            continue
        elif name == "q":
            break

        tracks = None
        try:
            tracks = s.search(name)
        except Exception as e:
            print("Error! reconnecting...")
            s.reload_on_error()
            tracks = s.search(name)

        if len(tracks) <= 0:
            print("No result...")
            continue

        show_tracks(tracks)

        track_number = 0
        try:
            track_number = int(input(f"Which track 1-{str(len(tracks))} (or enter to continue): "))
        except ValueError as verr:
            continue

        s.enqueue_track(tracks[track_number-1])
        print()
        print()


if __name__ == "__main__":
    try:
        ask_user()
    except KeyboardInterrupt as k:
        print("Exit....")
