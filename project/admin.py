from flask import request, Blueprint, render_template, redirect, url_for
from flask_login import login_required, current_user
from . import db
from . import spotify
from .models import User

admin = Blueprint('admin', __name__)

@admin.route('/admin')
@login_required
def admin_base():
    devices = spotify.get_devices()
    device_id = request.args.get('device')
    spotify.set_device(device_id)
    users = User.query.all()
    return render_template("admin.html", 
                           user=current_user.username,
                           len=len(devices),
                           devices=devices,
                           len_users=len(users),
                           users=users)

@admin.route('/remove')
@login_required
def remove():
    uri = request.args.get('uri')
    spotify.delete_track(uri)
    return redirect(url_for("main.music_queue"))


@admin.route("/next")
@login_required
def next():
    spotify.skip_track()
    return redirect(url_for("admin.admin_base"))