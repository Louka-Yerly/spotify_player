import spotipy
from spotipy import SpotifyOAuth
import spotipy.util as util
from .my_track import Track
import enum
from .my_spotify_queue import SpotifyQueue
 
from threading import Lock, RLock
from time import sleep
import time
from .cred import Cred

class STATE(enum.Enum):
    PLAYING = 0
    PAUSE   = 1


class Spot():

    def __init__(self):
        self.__CLIENT_ID = Cred.CLIENT_ID
        self.__CLIENT_SECRET = Cred.CLIENT_SECRET
        self.__redirect_uri = Cred.redirect_uri
        self.__username = Cred.username
        self.__scope = ["user-read-playback-state", "user-read-playback-state", "user-modify-playback-state"]
        #token = util.prompt_for_user_token(self.__username, 
        #                                   self.__scope,
        #                                   self.__CLIENT_ID,
        #                                   self.__CLIENT_SECRET,
        #                                   self.__redirect_uri)
        #self.spotify = spotipy.Spotify(auth=token)
        self.spotify = None
        self.__reload_on_error()
        self.__queue = SpotifyQueue()
        #self.__thread = threading.Thread(target=self.__player, args=())
        #self.__thread.setDaemon(True)
        #self.__thread.start()
        self.__lock = Lock()
        self.__end_time = time.time()
        self.__lock_device = RLock()
        self.__device = "6cc11bff9b5847034395a4eb1e0b1ea108c79827"
    
    def __reload_on_error(self):
        auth_manager = SpotifyOAuth(client_id=self.__CLIENT_ID,
                                    client_secret=self.__CLIENT_SECRET,
                                    username=self.__username,
                                    redirect_uri=self.__redirect_uri,
                                    scope=self.__scope)
        self.spotify = spotipy.Spotify(oauth_manager=auth_manager)
    
    def __set_end_time(self, t_s):
        self.__lock.acquire()
        self.__end_time = t_s
        self.__lock.release()
    
    def __get_end_time(self) -> float:
        t_s = float()
        self.__lock.acquire()
        t_s = self.__end_time
        self.__lock.release()
        return t_s 

        
    def search(self, title: str) -> list:
        try:
            results = self.spotify.search(q=title,
                                        type="track",
                                        market="CH")
        except Exception as e:
            self.__reload_on_error()
            results = self.spotify.search(q=title,
                                        type="track",
                                        market="CH")

        data = results["tracks"]["items"]
        return self.get_tracks(data)

    def get_tracks(self, data: list) -> list:
        tracks = list()
        for i in range(len(data)):
            tracks.append(Track(artist=data[i]["artists"][0]["name"],
                               track=data[i]["name"],
                               album=data[i]["album"]["name"],
                               uri=data[i]["uri"],
                               album_image=data[i]["album"]["images"][0]["url"],
                               duration=data[i]["duration_ms"]))
        return tracks
    
    def track_from_uri(self, uri: str):
        uri = uri.split(":")
        try:
            result = self.spotify.track(uri[2], "CH")
        except Exception as e:
            self.__reload_on_error()
            result = self.spotify.track(uri[2], "CH")
        return self.get_tracks([result, ])[0]
        
    
    def __play(self, track : Track):
        device_id = self.__get_device()
        try:
            self.spotify.start_playback(device_id=device_id, uris=[track.uri, ])
        except Exception as e:
            self.__reload_on_error()
            self.spotify.start_playback(device_id=device_id, uris=[track.uri, ])
    
    
    def __pause(self):
        try:
            self.spotify.pause_playback(self.__get_device())
        except Exception as e:
            self.__reload_on_error()
            self.spotify.pause_playback(self.__get_device())

    def __transfert(self, device_id):
        try:
            self.spotify.transfer_playback(device_id)
        except Exception as e:
            self.__reload_on_error()
            self.spotify.transfer_playback(device_id)

    def skip_track(self):
        self.__set_end_time(time.time()-1)
        if self.__queue.isEmpty():
            self.__pause()    
         
    def delete_track(self, uri):
        self.__queue.remove(uri)

    def get_devices(self):
        devices = {"devices":[]}
        try:
            devices = self.spotify.devices()
        except Exception as e:
            self.__reload_on_error()
            devices = self.spotify.devices()
        return devices["devices"]

    def set_device(self, id):
        devices = self.get_devices()
        self.__lock_device.acquire()
        for device in devices:
            if device["id"] == id and id != self.__device:
                self.__device = id
                self.__transfert(id)
                break
        self.__lock_device.release()

    def __get_device(self) -> str:
        devices = self.get_devices()
        current_device = devices[0]["id"]
        self.__lock_device.acquire()
        for device in devices:
            if device["id"] == self.__device:
                current_device = device["id"]
                break
        self.__lock_device.release()
        return current_device


    def enqueue_track(self, track: Track) -> None:
        self.__queue.put(track)


    def get_queue(self):
        return self.__queue.get_queue()

    def player(self):
        print("Starting player...")
        while(True):
            if not self.__queue.isEmpty():
                track = self.__queue.get()
                self.__play(track)
                self.__set_end_time(time.time()+(track.duration+50)/1000)
                while time.time() < self.__get_end_time():
                    sleep(0.05)
                continue
            sleep(0.5)


