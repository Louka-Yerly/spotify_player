from .my_track import Track
from threading import Lock


class SpotifyQueue():
    lock_list = Lock()

    def __init__(self) -> None:
        self.__lock = Lock()
        self.__queue = list()

    def remove(self, uri):
        self.__lock.acquire()
        for i in range(len(self.__queue)):
            if self.__queue[i].uri == uri:
                self.__queue.remove(self.__queue[i])
                break
        self.__lock.release()
    
    def put(self, track: Track):
        self.__lock.acquire()
        self.__queue.append(track)
        self.__lock.release()

    def get(self) -> Track:
        track = None
        self.__lock.acquire()
        if len(self.__queue) > 0:
            track = self.__queue[0]
            self.__queue.remove(track)
        self.__lock.release()
        return track
    
    def get_queue(self):
        result = list()
        self.__lock.acquire()
        for i in range(len(self.__queue)):
            result.append(self.__queue[i])
        self.__lock.release()
        return result

    def isEmpty(self):
        empty = True
        self.__lock.acquire()
        empty = len(self.__queue) <= 0
        self.__lock.release()
        return empty