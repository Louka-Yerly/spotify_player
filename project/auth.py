from flask import Blueprint, render_template, redirect, url_for, request, flash
from flask_login import login_user, logout_user, login_required, current_user
from werkzeug.security import generate_password_hash, check_password_hash
from .models import User
from . import db

auth = Blueprint('auth', __name__)

def reset_to_default_user():
    users = User.query.all()
    for user in users:
        db.session.delete(user)
    new_user = User(username="test",
                    password=generate_password_hash("123", method='sha256'))
    db.session.add(new_user)
    db.session.commit()


@auth.route('/login')
def login():
    if User.query.count() < 1:
        reset_to_default_user()
    return render_template('login.html')

@auth.route('/login', methods=['POST'])
def login_post():
    username = request.form.get("username")
    password = request.form.get("password")
    remember = True if request.form.get('remember') else False
    user = User.query.filter_by(username=username).first()

    if not user or not check_password_hash(user.password, password):
        flash('Please check your login details and try again.')
        return redirect(url_for('auth.login'))
    
    # user has correct credential
    login_user(user, remember=remember)
    return redirect(url_for('admin.admin_base'))


@auth.route('/users')
@login_required
def signup():
    users = User.query.all()
    return render_template('users.html')

@auth.route('/users', methods=['POST'])
@login_required
def signup_post():
    username = request.form.get('username')
    password = request.form.get('password')
    super_admin = True if request.form.get('super_admin') else False

    user = User.query.filter_by(username=username).first() # if this returns a user, then the email already exists in database

    if user: # if a user is found, we want to redirect back to signup page so user can try again
        flash('username already exists')
        return redirect(url_for('auth.signup'))

    # create a new user with the form data. Hash the password so the plaintext version isn't saved.
    new_user = User(username=username, password=generate_password_hash(password, method='sha256'), super_admin=super_admin)

    # add the new user to the database
    db.session.add(new_user)
    db.session.commit()

    return redirect(url_for('auth.login'))



@auth.route('/account')
@login_required
def account():
    username = request.args.get('username')
    if (current_user.super_admin) and (User.query.count() > 1) and current_user.username != username:
        user = User.query.filter_by(username=username).first()
        db.session.delete(user)
        db.session.commit()
    return redirect(url_for("admin.admin_base"))


@auth.route('/logout')
def logout():
    logout_user()
    return render_template("index.html")