from flask import redirect, request, Blueprint, render_template, url_for
from . import db
from . import spotify

main = Blueprint('main', __name__)

@main.route('/')
def index():
    return render_template("index.html")


@main.route('/list', methods=['POST'])
def list_music():
    music = request.form['music']
    if music == None or music == "":
        return redirect(url_for("main.index"))
    tracks = spotify.search(music)
    return render_template("music_list.html", len=len(tracks), tracks=tracks)


@main.route('/enqueue', methods=['GET'])
def enqueue():
    uri = request.args.get('uri')
    spotify.enqueue_track(spotify.track_from_uri(uri))
    return render_template("index.html")

@main.route('/queue')
def music_queue():
    tracks=spotify.get_queue()
    return render_template("music_queue.html", len=len(tracks), tracks=tracks)

@main.route('/profile')
def profile():
    return render_template('profile.html')